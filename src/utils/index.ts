import {readFileSync} from "fs";
import parse from "csv-parse/lib/sync";

export const loadCsv = (path: string) => {
  console.log(`Loading file from ${path}`);
  const csv = readFileSync(path);

  const result = parse(csv, {columns: true});

  return result;
};
