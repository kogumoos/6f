import express from "express";
import * as routes from "./routes";
import {loadRoutes, Route} from "./operations/loadRoutes";
import {loadAirports} from "./models/airports";
import {prepareFlights} from "./operations/prepareFlights";

const app = express();
const port = 3000;
const hostname = "localhost";

process.on("unhandledRejection", ({message, stack}) => {
  console.error("uncaughtException", {message, stack});
  setTimeout(() => {
    throw {message, stack};
  }, 1000);
});

async function init() {
  loadAirports();
  const airRoutes: Route[] = loadRoutes();
  prepareFlights(airRoutes);

  routes.init(app);

  return app.listen(port, hostname, () => {
    console.log(`Server listening on, ${hostname}:${port}`);
  });
}

(async () => {
  await init()
    .then(() => {
      console.info(`Everything is set up!`);
    })
    .catch(({message, stack}) => {
      console.info(`Server failed to start up`);
      throw {message, stack};
    });
})();
