import {getDistance} from "geolib";
import {loadCsv} from "../utils";
import {Airport, getAirportByCode} from "../models/airports";

export interface Route {
  source: string;
  destination: string;
  distance: number;
}

export const loadRoutes = (): Route[] => {
  const data = loadCsv("./routes.dat");
  const routes: Route[] = [];

  data.forEach((item) => {
    const source: Airport = getAirportByCode(item.Source);
    const destination: Airport = getAirportByCode(item.Destination);

    if (source && destination) {
      const distance: number = getDistance(
        {latitude: source.Latitude, longitude: source.Longitude},
        {latitude: destination.Latitude, longitude: destination.Longitude},
        1000
      );

      routes.push(<Route>{source: source.ICAO, destination: destination.ICAO, distance});
    }
  });

  return routes;
};
