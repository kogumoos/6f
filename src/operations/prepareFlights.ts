import createGraph, {Graph, Node, Link} from "ngraph.graph";
import {Route} from "./loadRoutes";

export const flights: Graph = createGraph();

export const prepareFlights = (routes: Route[]) => {
  console.log("Preparing Flights");

  routes.map((route: Route) => flights.addLink(route.source, route.destination, {weight: route.distance}));
};
