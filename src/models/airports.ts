import {loadCsv} from "../utils";
import {get} from "lodash";

let airports: Airports = {};
let airportsHashmap = {};

export interface Airports {
  [key: string]: Airport;
}
export interface Airport {
  Id: number;
  Name: string;
  IATA: string;
  ICAO: string;
  Latitude: number;
  Longitude: number;
  Altitude: number;
  Timezone: number;
  DST: string;
  TimezoneDatabase: string;
  Type: string;
  Source: string;
}

export const loadAirports = () => {
  const data = loadCsv("./airports.dat");

  data.forEach((item) => {
    airports[item.ICAO] = <Airport>item;
    airportsHashmap[item.IATA] = <string>item.ICAO;
  });
};

export const getAirportByCode = (code): Airport | null => {
  if (code.length === 3) {
    return get(airports, airportsHashmap[code.toUpperCase()], null);
  } else if (code.length === 4) {
    return get(airports, code.toUpperCase(), null);
  } else {
    return null;
  }
};
