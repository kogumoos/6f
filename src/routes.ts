import {Application} from "express";
import {getAirports} from "./middleware/getAirports";
import {findShortestPath} from "./middleware/findShortestPath";
import {prepareOutput} from "./middleware/prepareData";

const routes = [["get", "/:source/:destination", getAirports, findShortestPath, prepareOutput]];

export const init = (app: Application) => {
  registerRoutes(app, routes);
};

const registerRoutes = (app: Application, routes: any[]) => {
  routes.forEach((route: any[]) => {
    const method = route.shift();
    const path = route.shift();

    app[method](path, ...route);
  });
};
