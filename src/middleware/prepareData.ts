import {Request, Response} from "express";
import {Airport, getAirportByCode} from "../models/airports";

export const prepareOutput = (req: Request, res: Response) => {
  const {data} = res.locals;

  try {
    const result: Airport = data.reverse().map((item, index) => getAirportByCode(item.id));

    return res.json({
      input: {...req.params},
      legs: data.length,
      data: result,
    });
  } catch (e) {
    console.log("Error mapping output", e);

    return res.json({
      message: `Could not map output`,
      e,
    });
  }
};
