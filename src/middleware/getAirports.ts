import {NextFunction, Request, Response} from "express";
import {Airport, getAirportByCode} from "../models/airports";

export const getAirports = async (req: Request, res: Response, next: NextFunction) => {
  const {source, destination} = req.params;

  try {
    console.log(`Looking for ${source} and ${destination}`);

    const sourceId: Airport = getAirportByCode(source);
    const destinationId: Airport = getAirportByCode(destination);

    res.locals = {
      source: sourceId.ICAO,
      destination: destinationId.ICAO,
    };

    return next();
  } catch (e) {
    console.log(`Could not find`, {source, destination});

    return res.json({
      message: `Could not find airports for ${source} and ${destination}`,
      e,
    });
  }
};
