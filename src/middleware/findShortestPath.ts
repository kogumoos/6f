import {NextFunction, Request, Response} from "express";
import path from "ngraph.path";
import {flights} from "../operations/prepareFlights";

export const findShortestPath = (req: Request, res: Response, next: NextFunction) => {
  const {source, destination} = res.locals;

  try {
    console.log("Finding the shortest path", source, destination);

    const shortestPath = shortestPathViaDistance(source, destination);
    const data = shortestPath.length > 4 ? shortestPathViaLegs(source, destination) : shortestPath;

    res.locals["data"] = data;

    return next();
  } catch (e) {
    return res.json({
      message: `Could not find path for ${source} and ${destination}`,
      e,
    });
  }
};

const shortestPathViaDistance = (source: string, destination: string) => {
  const pathFinder = path.aStar(flights, {
    distance(fromNode, toNode, link) {
      return link.data.weight;
    },
  });

  return pathFinder.find(source, destination);
};

const shortestPathViaLegs = (source: string, destination: string) => {
  const pathFinder = path.aStar(flights);

  return pathFinder.find(source, destination);
};
