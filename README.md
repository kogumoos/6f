# Flight router

## Description

This is a JSON over HTTP API for querying simple flight routing information.

Firstly the test-data is loaded from CSV files and stored in memory.

After data is loaded, a graph representation is built using Airports as nodes and Routes between them as edges.
It utilizes [ngraph.graph](https://github.com/anvaka/ngraph.graph) & [ngraph.path](https://github.com/anvaka/ngraph.path) from the [ngraph](https://github.com/anvaka/ngraph) toolbelt for building a graph and finding the shortest path. The distance between different airports was calculated from their coordinates via [geolib](https://github.com/manuelbieh/geolib)'s [getDistance()](https://github.com/manuelbieh/geolib#getdistancestart-end-accuracy--1) helper function.

- Libraries were chosen to reduce overhead on busywork of reimplementing algorithms from scratch.

Shortest path lookup is done via [A\*](https://en.wikipedia.org/wiki/A*_search_algorithm). In case the amount of layovers become unbearable (4), [Dijkstra's](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) search algorithm is used as a fallbacking option.

Finally the output is mapped to an eye friendly format.

## Requirements

- node `v10.10.0` or higher
- npm `v6.4.1` or higher

## To start the project

```shell script
nvm use
npm i
npm run start
```

## Endpoints

This API exposes one endpoint for querying the shortest path between 2 given airport's `ICAO` or `IATA` code.

`GET - /:source/:destination`

#### Example

`http://localhost:3000/TLL/RIX`

Will yield the user:

```json
{
  "input": {"source": "TLL", "destination": "RIX"},
  "legs": 2,
  "data": [
    {
      "id": "415",
      "name": "Lennart Meri Tallinn Airport",
      "IATA": "TLL",
      "ICAO": "EETN",
      "latitude": "59.41329956049999",
      "longitude": "24.832799911499997",
      "meta": {"version": 0, "revision": 0, "created": 1579518566664},
      "$loki": 412
    },
    {
      "id": "3953",
      "name": "Riga International Airport",
      "IATA": "RIX",
      "ICAO": "EVRA",
      "latitude": "56.92359924316406",
      "longitude": "23.971099853515625",
      "meta": {"version": 0, "revision": 0, "created": 1579518566669},
      "$loki": 3746
    }
  ]
}
```
